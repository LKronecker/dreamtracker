//
//  DTLoginViewController.swift
//  DreamTracker
//
//  Created by Leopoldo Garcia Vargas on 2016-05-07.
//  Copyright © 2016 Leopoldo G Vargas. All rights reserved.
//

import Foundation
import UIKit

import FBSDKLoginKit

class DTLoginViewController : UIViewController, UITextFieldDelegate, FBSDKLoginButtonDelegate  {
    
    var loginView : FBSDKLoginButton = FBSDKLoginButton()
    
    override func viewDidLoad() {
    self.view.addSubview(loginView)
    loginView.readPermissions = ["public_profile", "email", "user_friends","user_birthday"]
    loginView.delegate = self
    self.addConstraintsToSubViews()
        
    self.view.backgroundColor = UIColor.blue
    
    }
    
    fileprivate func addConstraintsToSubViews() {
        
        self.loginView.mas_makeConstraints { make in
            // login Button
            make?.top.equalTo()(self.view.mas_top)?.offset()(50)
            make?.left.equalTo()(self.view.mas_left)?.offset()(10)
            make?.height.equalTo()(44)
            make?.width.equalTo()(200)
        }
    }
    
    // MARK .- FBSDKLoginButtonDelegate
    
    internal func loginButton(_ loginButton: FBSDKLoginButton!, didCompleteWith result: FBSDKLoginManagerLoginResult!, error: Error!) {
        if ((error) != nil)
        {
            //handle error
        } else {
            returnUserData()
        }
    }
    
    internal func loginButtonDidLogOut(_ loginButton: FBSDKLoginButton!) {
    
    }
    
    // MARK .- User data
    
    func returnUserData() {
        
        let graphRequest : FBSDKGraphRequest = FBSDKGraphRequest(graphPath: "me", parameters: ["fields":"id,interested_in,gender,birthday,email,age_range,name,picture.width(480).height(480)"])
        graphRequest.start(completionHandler: { (connection, result, error) -> Void in
            
            if ((error) != nil)
            {
                // Process error
                print("Error: \(error)")
            }
            else
            {
//                print("fetched user: \(result)")
//                let id : NSString = result.value(forKey: "id") as! String
//                print("User ID is: \(id)")
                //etc...
            }
        })
    }

}
