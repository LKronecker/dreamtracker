//
//  DTSleepAnalysis.swift
//  DreamTracker
//
//  Created by Leopoldo G Vargas on 2016-04-30.
//  Copyright © 2016 Leopoldo G Vargas. All rights reserved.
//

import Foundation
import UIKit
import HealthKit

class DTSleepAnalysis: UIViewController {

    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        // create our store
        
        let healthStore = HKHealthStore()
        
        // create an object type to request an authorization for a specific category, here is SleepAnalysis
        let sleepType = HKObjectType.categoryType(forIdentifier: HKCategoryTypeIdentifier.sleepAnalysis)
        
        healthStore.requestAuthorization(toShare: NSSet(object: sleepType!) as? Set<HKSampleType>, read: (NSSet(object: sleepType!) as! Set<HKObjectType>), completion: {(success, error) -> Void in
            
            
            // here is your code
            
            // first, we define the object type we want
            if let sleepType = HKObjectType.categoryType(forIdentifier: HKCategoryTypeIdentifier.sleepAnalysis) {
                
                //Modify these dates to analyse sleep data between them...
                let startDate = Date()
                let endDate = Date()
                
                // we create a predicate to filter our data
                let predicate = HKQuery.predicateForSamples(withStart: startDate,end: endDate ,options: HKQueryOptions())
                
                // I had a sortDescriptor to get the recent data first
                
                let sortDescriptor = NSSortDescriptor(key: HKSampleSortIdentifierEndDate, ascending: false)
                
                // we create our query with a block completion to execute
                
                let query = HKSampleQuery(sampleType: sleepType, predicate: predicate, limit: 30, sortDescriptors: [sortDescriptor]) { (query, tmpResult, error) -> Void in
                    
                    if error != nil {
                        
                        // something happened
                        return
                        
                    }
                    
                    if let result = tmpResult {
                        
                        // do something with my data
                        for item in result {
                            if let sample = item as? HKCategorySample {
                                
                                let value = (sample.value == HKCategoryValueSleepAnalysis.inBed.rawValue) ? "InBed" : "Asleep"
                                
                                print("Healthkit sleep: \(sample.startDate) \(sample.endDate) - source: \(sample.sourceRevision.source) - value: \(value)")
                            }
                        }
                    }
                }
                
                // finally, we execute our query
                healthStore.execute(query)
            }
            
            
            
            print("sleep analysis")
            
        })
        
    }
}
