//
//  ViewController.swift
//  DreamTracker
//
//  Created by Leopoldo G Vargas on 2015-11-24.
//  Copyright © 2015 Leopoldo G Vargas. All rights reserved.
//

import UIKit
import MessageUI

//import EQNotificationManager

class ViewController: UIViewController, MFMailComposeViewControllerDelegate {
    // motion Vars
//    let motionManager: CMMotionManager = CMMotionManager()
//    var accelerationX: CGFloat = CGFloat()
    // csv creation vars
    // Variables
    var toDoItems:[String] = []
    var convertMutable: NSMutableString!
    var incomingString: String = ""
    var datastring: NSString!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        self.view.backgroundColor = UIColor.red;
        
        

        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
//// Move to EQMotionDectectorService
/////////////////////////////Accelerometer///////////////////////////////////////
//    func startAccelerometer() {
//        
//        let _:CMAccelerometerData!
//        let _:NSError!
//        
//        if(motionManager.accelerometerAvailable){
//            motionManager.startAccelerometerUpdatesToQueue(NSOperationQueue.currentQueue()!, withHandler: {
//                accelerometerData,error in
//                
//                let acceleration = accelerometerData!.acceleration
//                self.accelerationX = CGFloat(acceleration.x)
//                
//                // Rotate view
////                let rotation = atan2(accelerometerData!.acceleration.x, accelerometerData!.acceleration.y) - M_PI
////                self.view!.transform = CGAffineTransformMakeRotation(CGFloat(rotation))
//                
//                self.refineData((accelerometerData?.description)!)
//            })
//        }
//    }
//    
//    func stopAccelerometer() {
//        motionManager.stopAccelerometerUpdates()
//    }
//    
/////////////////////////////Gryroscope///////////////////////////////////////
//    func startGyro() {
//        if motionManager.gyroAvailable {
//            motionManager.gyroUpdateInterval = 0.1
//            motionManager.startGyroUpdates()
//            
//            let queue = NSOperationQueue.mainQueue
//            motionManager.startGyroUpdatesToQueue(queue()) {
//                (data, error) in
//                
//                self.refineData((data?.description)!)
//            }
//        }
//    }
//    
//    func stopGyro() {
//        motionManager.stopGyroUpdates()
//        }

///////////////////////////Refine data///////////////////////////////////////
//    func refineData(data: NSString) {
//        print(data)
//        
//        _ = NSMakeRange(0, data.length)
//        
//        self.toDoItems.append(data as String)
//    }

///////////////////////////Export/////////////////////////////////////////
    
    @IBAction func startListening(_ sender: AnyObject) {
//        self.startGyro()
//        self.startAccelerometer()
    }
    
    @IBAction func csvExport(_ sender: AnyObject) {
        // Convert tableView String Data to NSMutableString
        convertMutable = NSMutableString();
        for item in toDoItems
        {
            convertMutable.appendFormat("%@\r", item)
        }
        
        print("NSMutableString: \(convertMutable)")
        
        // Convert above NSMutableString to NSData
        let data = convertMutable.data(using: String.Encoding.utf8.rawValue, allowLossyConversion: false)
        if let d = data { // Unwrap since data is optional and print
            print("NSData: \(d)")
        }
        
        //Email Functions
        func configuredMailComposeViewController() -> MFMailComposeViewController {
            let mailComposerVC = MFMailComposeViewController()
            mailComposerVC.mailComposeDelegate = self
            mailComposerVC.setSubject("CSV File Export")
            mailComposerVC.setMessageBody("", isHTML: false)
            mailComposerVC.addAttachmentData(data!, mimeType: "text/csv", fileName: "TodoList.csv")
            
            return mailComposerVC
        }
        
        // Compose Email
        let mailComposeViewController = configuredMailComposeViewController()
        if MFMailComposeViewController.canSendMail() {
            self.present(mailComposeViewController, animated: true, completion: nil)
        } else {
            self.showSendMailErrorAlert() // One of the MAIL functions
        }
    }
    
    // Mail alert if user does not have email setup on device
    func showSendMailErrorAlert() {
        let sendMailErrorAlert = UIAlertView(title: "Could Not Send Email", message: "Your device could not send e-mail.  Please check e-mail configuration and try again.", delegate: self, cancelButtonTitle: "OK")
        sendMailErrorAlert.show()
    }
    // MARK: MFMailComposeViewControllerDelegate Method
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
    }

    
    
}
