//
//  DTDatabaseService.swift
//  DreamTracker
//
//  Created by Leopoldo Garcia Vargas on 2016-05-07.
//  Copyright © 2016 Leopoldo G Vargas. All rights reserved.
//

import Foundation
import RealmSwift

class DTDatabaseService {
    
    let realm = try! Realm()    
    class var sharedInstance:DTDatabaseService {
        struct Singleton {
            static let instance = DTDatabaseService()
        }
        return Singleton.instance
    }
    
    func persistDream(dream:DTDreamStorageModel) {
        try! realm.write {
            realm.add(dream)
        }
    }
    
    func retreivedreamList () -> [DTDreamStorageModel] {
        return Array(realm.objects(DTDreamStorageModel.self))
    }
    
    func persistRealityCheck(realityCheck:DTRealityCheckStorageModel) {
        try! realm.write {
            realm.add(realityCheck)
        }
    }
    
    func persistRealityChecks (realityCheckList: [DTRealityCheckMappingModel]){
        for rt in realityCheckList {
            let realityToStore = DTRealityCheckStorageModel()
            realityToStore.id = rt.id
//            realityToStore.title = rt.title
            realityToStore.body = rt.body
            realityToStore.image = rt.image
            
            self.persistRealityCheck(realityCheck: realityToStore)
        }
    }
    
    func retreiveRealityCheckList () -> [DTRealityCheckStorageModel] {
        return Array(realm.objects(DTRealityCheckStorageModel.self))
    }
}
