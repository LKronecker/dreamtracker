//
//  DTDataBroker.swift
//  DreamTracker
//
//  Created by Leopoldo Garcia Vargas on 2016-09-27.
//  Copyright © 2016 Leopoldo G Vargas. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireImage
import AlamofireObjectMapper
import SystemConfiguration

typealias ServiceResponse = (NSDictionary?, NSError?) -> Void

class DTDataBroker: NSObject {
    
    let BASE_URL = "http://localhost:3000/"
    
    class var sharedInstance:DTDataBroker {
        struct Singleton {
            static let instance = DTDataBroker()
        }
        return Singleton.instance
    }
    
    func getDreams(onCompletion: @escaping ServiceResponse){
        let URL = String(format: "%@%@", self.BASE_URL, "dreams")
        Alamofire.request(URL).responseArray { (response: DataResponse<[DTDreamMappingModel]>) in
            
            if (response.result.value != nil) {
                let responseDict = ["dreams":response.result.value!] as NSDictionary
                onCompletion(responseDict, nil)
            }
            else {
                onCompletion(nil, NSError())
            }
        }
    }
    
    func postDream(dream:DTDreamStorageModel, onCompletion: @escaping ServiceResponse){
        let URL = String(format: "%@%@", self.BASE_URL, "dreams")
        
//        let dreamDict = ["name": dream.name, "body": dream.body, "date": dream.date, "lucid": dream.lucid, "notes": dream.notes!] as [String : String]
        
        let dateformatter = DateFormatter()
        dateformatter.dateStyle = DateFormatter.Style.short
        dateformatter.timeStyle = DateFormatter.Style.short
        let dreamDate = dateformatter.string(from: dream.date)
        
        let dreamDict = ["_id":dream.id, "name": dream.name, "body": dream.body, "date": dreamDate, "lucid": dream.lucid.description, "notes": dream.notes] as [String : String]

        Alamofire.request(URL, method: .post, parameters: dreamDict, encoding: JSONEncoding.default, headers: nil).responseJSON { (response:DataResponse<Any>) in
            
            switch(response.result) {
            case .success(_):
                if response.result.value != nil{
//                    print(response.result.value)
                    onCompletion(response.result.value as! NSDictionary?, nil)
                }
                break
                
            case .failure(_):
//                print(response.result.error)
                break
            }
        }
    }
    
    func getRealityChecks(onCompletion: @escaping ServiceResponse){
        
        let interntAvailable = self.isInternetAvailable()
        print(interntAvailable)
        
        if interntAvailable {
            let URL = String(format: "%@%@", self.BASE_URL, "realitychecks")
            Alamofire.request(URL).responseArray { (response: DataResponse<[DTRealityCheckMappingModel]>) in
                
                if (response.result.value != nil) {
                    let responseDict = ["realitychecks":response.result.value!] as NSDictionary
                    onCompletion(responseDict, nil)
                }
                else {
                    onCompletion(nil, NSError())
                }
            }
        }
        else {
            print("Parse local json")
            let path = Bundle.main.path(forResource: "reality_check_en", ofType: "json")
            print(path as Any);
            
            if !(path != nil) {
                print("No reality check json found")
                return
            }

            let stringData = try? String(contentsOfFile: path!, encoding: String.Encoding.utf8) 
            print(stringData as Any)
        }
    }
    
    func isInternetAvailable() -> Bool {
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
                SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
            }
        }
        
        var flags = SCNetworkReachabilityFlags()
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) {
            return false
        }
        let isReachable = (flags.rawValue & UInt32(kSCNetworkFlagsReachable)) != 0
        let needsConnection = (flags.rawValue & UInt32(kSCNetworkFlagsConnectionRequired)) != 0
        return (isReachable && !needsConnection)
    }

}
