//
//  DTNotificationSettingsView.swift
//  DreamTracker
//
//  Created by Leopoldo G Vargas on 2016-01-04.
//  Copyright © 2016 Leopoldo G Vargas. All rights reserved.
//

import UIKit

typealias RegisterNotification = (_ notificationsPerDay : Int) -> Void

@IBDesignable class DTNotificationSettingsView: UIView {
    
    // notification
    let notification = EQNotificationManager()
    fileprivate let kLocalNotificationMessage:String = "Do a reality check now!!!"
    
    @IBOutlet weak var notificationTextField: UITextField?
    var registerNotification:RegisterNotification?
    var view: UIView!
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        view = loadViewFromNib()
        addSubview(view)
    }
    
    func loadViewFromNib() -> UIView {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: "DTNotificationSettingsView", bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        
        return view
    }
    
    // MARK: IBactions
    @IBAction func save(_ sender: UIButton) {
        let notifNumber = Int((notificationTextField?.text!)!)
        scheduleNotifications(notifNumber!)
        notificationTextField?.resignFirstResponder()
        
    }
    
        // MARK: Notifications
    func scheduleNotifications (_ numberOfNotifications: NSInteger) {
        notification.ScheduleLocalNotificationIfPossible(kLocalNotificationMessage, numberOfNotifications:numberOfNotifications)
    }
    
    // MARK: IB code
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        view = loadViewFromNib()
        addSubview(view)
    }
    
}
