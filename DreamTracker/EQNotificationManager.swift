//
//  EQNotificationManager.swift
//  DreamTracker
//
//  Created by Leopoldo G Vargas on 2015-12-07.
//  Copyright © 2015 Leopoldo G Vargas. All rights reserved.
//

import Foundation
import UIKit

class EQNotificationManager {

    var repetitions: Double = Double()
    var numberOfNotifications: NSInteger = NSInteger()
    var listOfIntevals: NSArray = NSArray()
    
    fileprivate let kLocalNotificationTimeInterval:TimeInterval = 5
    
    init() {
        
    }
    
    init(fromSettings reps: Double) {
        repetitions = reps
    }
    
    fileprivate func LocalNotification(_ timeInterval: TimeInterval, notificationMessage: String) -> UILocalNotification {
        let localNotification:UILocalNotification = UILocalNotification()
        localNotification.fireDate = Date(timeIntervalSinceNow:timeInterval)
        localNotification.alertBody = notificationMessage
        return localNotification
    }
    
    // Date logics
    fileprivate func createInterval(_ numberOfNotifications: NSInteger, index: NSInteger) -> TimeInterval {
        
        let maxHour = 24
        let now = Date()
    
        let calendar = Calendar.current
        let components = (calendar as NSCalendar).components([.month, .day, .hour], from: now)
        let remainingSeconds = (maxHour - components.hour!)*60*60
        let splitSeconds = remainingSeconds/numberOfNotifications
        
        var interval = TimeInterval()
        interval = Double(splitSeconds * index)
        
        // Delay notification for 13 seconds when scheduled right away
        if interval == 0 {
            interval = 13.0
        }
        
        return interval
    }
    
    // Schedule notifications public
    func ScheduleLocalNotificationIfPossible(_ message: String, numberOfNotifications: NSInteger) {
        for i in 0 ..< numberOfNotifications {
            UIApplication.shared.scheduleLocalNotification(LocalNotification(self.createInterval(numberOfNotifications, index: i), notificationMessage: message))                }
        }
}
