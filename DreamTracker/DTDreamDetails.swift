//
//  DTDealDetails.swift
//  DreamTracker
//
//  Created by Leopoldo G Vargas on 2016-04-27.
//  Copyright © 2016 Leopoldo G Vargas. All rights reserved.
//

import Foundation
import UIKit

import RealmSwift

protocol DTDreamDetailsProtocol {
    func refreshController()
}

class DTDreamDetails: UIViewController {
    
    weak var delegate : DTDreamJournal?
    var dream: DTDreamStorageModel;
    //Outlets
    @IBOutlet var dreamTitle: UITextField!
    @IBOutlet var dreamBody: UITextView!
    @IBOutlet var dreamNotes: UITextView!
    @IBOutlet var luciditySwitch: UISwitch!
    
    var saveEditButton : UIBarButtonItem!
    
    var isEditingMode : Bool!
    
    // MARK.- Initializers
    
    required init(coder aDecoder: (NSCoder!)) {
        self.dream = DTDreamStorageModel()
        super.init(coder: aDecoder)!
    }
    
    override init(nibName nibNameOrNil: String!, bundle nibBundleOrNil: Bundle!) {
        self.dream = DTDreamStorageModel()
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    convenience init(dream:DTDreamStorageModel) {
        self.init(nibName: nil, bundle: nil)
        self.dream = dream

    }
    
    // MARK .- Lifecycvar
    override func viewDidLoad() {
        
        if (self.dream.name != "") { // dream is passed
            self.isEditingMode = false
            
            self.dreamTitle.text! = self.dream.name
            self.dreamBody.text! = self.dream.body
            
            self.toggleEditableUI(false)
        }
        else {
            print ("editable")
            self.isEditingMode = true
        }
        
        self.saveEditButton = self.createBarButton()
        navigationItem.rightBarButtonItem = self.saveEditButton
    }
    
    // MARK .- Custom methods
     func saveDream() {
        self.dream.name = self.dreamTitle.text!
        self.dream.body = self.dreamBody.text!
        self.dream.notes = self.dreamNotes.text!
        self.dream.lucid = true
        self.dream.createdID()
        self.dream.createDate()
        
        // write to database
        DTDatabaseService.sharedInstance.persistDream(dream: self.dream)
        DTDataBroker.sharedInstance.postDream(dream: self.dream) { (NSDictionary, NSError) in
            print(NSDictionary)
        }
        
        self.delegate!.refreshController()
        self.navigationController?.popViewController(animated: true)
    }
    
    func toggleEditableUI(_ isEditable:Bool) {
        self.isEditingMode = isEditable
        
        self.dreamTitle.isUserInteractionEnabled = isEditable
        self.dreamBody.isUserInteractionEnabled = isEditable
        self.dreamNotes.isUserInteractionEnabled = isEditable
    }
    
    func editSavedDream () {
        self.toggleEditableUI(true)
        self.saveEditButton = self.createBarButton()
        navigationItem.rightBarButtonItem = self.saveEditButton
    }
    
    // Button factory
    func createBarButton() -> UIBarButtonItem {
        if (isEditingMode == true) {
            return UIBarButtonItem(title: "Save", style: .plain, target: self, action: #selector(DTDreamDetails.saveDream as (DTDreamDetails) -> () -> ()))
        }
        else {
            return UIBarButtonItem(title: "Edit", style: .plain, target: self, action: #selector(DTDreamDetails.editSavedDream as (DTDreamDetails) -> () -> ()))
        }
    }
}
