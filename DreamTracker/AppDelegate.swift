//
//  AppDelegate.swift
//  DreamTracker
//
//  Created by Leopoldo G Vargas on 2015-11-24.
//  Copyright © 2015 Leopoldo G Vargas. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        application.registerUserNotificationSettings(UIUserNotificationSettings(types: [.sound, .alert, .badge], categories: nil))
        
        if DTDatabaseService.sharedInstance.retreiveRealityCheckList().count > 0 {
            print("Reality checks already loaded")
        }
        else {
            DTDataBroker.sharedInstance.getRealityChecks { (NSDictionary, NSError) in
                if ((NSError) != nil) {
                    // error
                } else {
                    let realityChecks = NSDictionary?["realitychecks"]
                    DTDatabaseService.sharedInstance.persistRealityChecks(realityCheckList: realityChecks as! [DTRealityCheckMappingModel])
                }
            }
        }
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }

    func application(_ application: UIApplication, didReceive notification: UILocalNotification) {
        // this is not working. figure out why
        if notification.alertBody == "Do a reality check now!!!" {
            let landingPage : DTRealityCheckLandingViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "landingpage") as UIViewController as! DTRealityCheckLandingViewController
            
            let realityCheckList = DTDatabaseService.sharedInstance.retreiveRealityCheckList()
            let randomIndex = Int(arc4random_uniform(UInt32(realityCheckList.count)))
            
            landingPage.realityCheck = realityCheckList[randomIndex]
            
            self.window?.rootViewController?.present(landingPage, animated: true, completion: {
                
            })
        }
        print(notification)
    }

}

