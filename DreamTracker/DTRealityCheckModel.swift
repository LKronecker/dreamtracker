//
//  DTRealityCheckModel.swift
//  DreamTracker
//
//  Created by Leopoldo G Vargas on 2016-04-30.
//  Copyright © 2016 Leopoldo G Vargas. All rights reserved.
//

import ObjectMapper

class DTRealityCheckMappingModel: Mappable {
    var id: String! = nil
    var title: String! = nil
    var body: String! = nil
    var image: String! = nil
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        id <- map["_id"]
        title <- map["name"]
        body <- map["body"]
        image <- map["image"]
    }
}

import RealmSwift

class DTRealityCheckStorageModel: Object {
    // @required
    dynamic var id = ""
    dynamic var title = ""
    dynamic var body = ""
    //@optional
    dynamic var image: String? = nil
    
}
