//
//  DTSettings.swift
//  DreamTracker
//
//  Created by Leopoldo G Vargas on 2016-01-04.
//  Copyright © 2016 Leopoldo G Vargas. All rights reserved.
//

import Foundation

import UIKit

class DTSettingsViewController: UIViewController, UICollectionViewDelegate{
    
    @IBOutlet var collectionView: UICollectionView!
//    let collectionViewLayout: DTRealityCheckCollectionVIewLayout = DTRealityCheckCollectionVIewLayout()
    let randomCheckReuseIdentifier = "randomCheckCell"
   
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Reality Checks"
        self.collectionView.register(UINib.init(nibName: "RandomRealityChecksCell", bundle: nil), forCellWithReuseIdentifier: randomCheckReuseIdentifier)
        self.collectionView.backgroundColor = UIColor.clear
//        self.collectionView.collectionViewLayout = collectionViewLayout
    }
    
    // MARK: - UICollectionViewDataSource protocol
    
    // tell the collection view how many cells to make
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 1
    }
    
    // make a cell for each cell index path
    func collectionView(_ collectionView: UICollectionView, cellForItemAtIndexPath indexPath: IndexPath) -> UICollectionViewCell {

        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: randomCheckReuseIdentifier, for: indexPath) as! RandomRealityChecksCell
        
        return cell
    }
    
    // MARK: - UICollectionViewDelegate protocol
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print("You selected cell #\((indexPath as NSIndexPath).item)!")
    }
    
    // Layout
    
    //Use for size
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                               sizeForItemAtIndexPath indexPath: IndexPath) -> CGSize {
        
        let screenSize: CGRect = UIScreen.main.bounds
        
        return CGSize(width: screenSize.width - 10, height: 170);
        
    }
    //Use for interspacing
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                               minimumInteritemSpacingForSectionAtIndex section: Int) -> CGFloat {
        return 1.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout
        collectionViewLayout: UICollectionViewLayout,
        minimumLineSpacingForSectionAtIndex section: Int) -> CGFloat {
        return 1.0
    }

    
}

//class DTRealityCheckCollectionVIewLayout: UICollectionViewFlowLayout {
//    
//    //Use for size
//    func collectionView(collectionView: UICollectionView,
//                        layout collectionViewLayout: UICollectionViewLayout,
//                               sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
//        
//        return CGSizeMake(300, 200);
//        
//    }
//    //Use for interspacing
//    func collectionView(collectionView: UICollectionView,
//                        layout collectionViewLayout: UICollectionViewLayout,
//                               minimumInteritemSpacingForSectionAtIndex section: Int) -> CGFloat {
//        return 1.0
//    }
//    
//    func collectionView(collectionView: UICollectionView, layout
//        collectionViewLayout: UICollectionViewLayout,
//        minimumLineSpacingForSectionAtIndex section: Int) -> CGFloat {
//        return 1.0
//    }
//}
