//
//  DTRealityCheckLandingViewController.swift
//  DreamTracker
//
//  Created by Leopoldo G Vargas on 2016-04-30.
//  Copyright © 2016 Leopoldo G Vargas. All rights reserved.
//

import Foundation
import UIKit


class DTRealityCheckLandingViewController: UIViewController {
    
    var realityCheck = DTRealityCheckStorageModel()
    
    @IBOutlet var testLabel : UILabel!
    @IBOutlet var closeButton : UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = UIColor.gray
        
        self.customizeSubViews()
    }
    
    fileprivate func customizeSubViews() {
        self.testLabel.text = self.realityCheck.body
        self.testLabel.backgroundColor = UIColor.red
    }
    
    // Mark - Actions
    
    @IBAction func dismiss(sender: UIButton) {
        print("dismiss")
        self.dismiss(animated: true) { 
            
        }
    }
}
