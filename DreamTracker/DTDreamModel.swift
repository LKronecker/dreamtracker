//
//  DTDreamModel.swift
//  DreamTracker
//
//  Created by Leopoldo G Vargas on 2016-04-26.
//  Copyright © 2016 Leopoldo G Vargas. All rights reserved.
//

import ObjectMapper

class DTDreamMappingModel: Mappable {
    var id: String! = nil
    var name: String! = nil
    var body: String! = nil
    var date: String! = nil
    //@optional
    var lucid: String! = nil
    var notes: String! = nil
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        id <- map["_id"]
        name <- map["name"]
        body <- map["body"]
        date <- map["date"]
        lucid <- map["lucid"]
        notes <- map["notes"]
    }
}

import RealmSwift

//class DTDreamStorageModel: Object {
//    // @required
//    dynamic var id = ""
//    dynamic var name = ""
//    dynamic var body = ""
//    dynamic var date = ""
//    //@optional
//    let lucid = ""
//    dynamic var notes: String? = nil
//    
//    //    func createDate() {
//    //        self.date = Date()
//    //    }
//}

class DTDreamStorageModel: Object {
    // @required
    dynamic var id = ""
    dynamic var name = ""
    dynamic var body = ""
    dynamic var date = Date()
    //@optional
//    var lucid = RealmOptional<Bool>()
    var lucid = true
    dynamic var notes = ""
    
    func createDate() {
        self.date = Date()
    }
    
    func createdID(){
        self.id = NSUUID().uuidString
    }
}
