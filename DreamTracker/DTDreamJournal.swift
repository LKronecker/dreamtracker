//
//  DTDreamJournal.swift
//  DreamTracker
//
//  Created by Leopoldo G Vargas on 2016-01-04.
//  Copyright © 2016 Leopoldo G Vargas. All rights reserved.
//

import UIKit
import RealmSwift

class DTDreamJournal: UIViewController , UITableViewDelegate, DTDreamDetailsProtocol {
    
    @IBOutlet var tableView: UITableView!
    @IBOutlet var dreamCountLabel: UILabel!
    var dreamList = [DTDreamStorageModel]()
    
// Life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.dreamList = DTDatabaseService.sharedInstance.retreivedreamList()
        self.title = "Dream Journal"
        self.tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.dreamCountLabel.text = String(format: "%i dreams", self.dreamList.count)
    }
    
// IBactions
    
    @IBAction func createNewDream(_ sender: AnyObject) {
        let testDream: DTDreamStorageModel = DTDreamStorageModel()
        self.presentDreamDetailViewController(testDream)
    }
    
// View Controller presentation
    
    func presentDreamDetailViewController(_ dream:DTDreamStorageModel) {
        
        //Push to navigation stack
        let storyboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let dreamDetailsVC = storyboard.instantiateViewController(withIdentifier: "DreamDetails") as! DTDreamDetails
        dreamDetailsVC.dream = dream;
        dreamDetailsVC.delegate = self
        
        self.navigationController?.pushViewController(dreamDetailsVC, animated: true)
    }
    
// Table view methods
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.dreamList.count;
    }
        
    func tableView(_ tableView: UITableView, cellForRowAtIndexPath indexPath: IndexPath) -> UITableViewCell {
        let cell:UITableViewCell = self.tableView.dequeueReusableCell(withIdentifier: "cell")! as UITableViewCell
        
        let dream = self.dreamList[(indexPath as NSIndexPath).row]
        cell.textLabel?.text = dream.name
        
        return cell
    }
        
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let dream = self.dreamList[(indexPath as NSIndexPath).row]
        self.presentDreamDetailViewController(dream)
    }
    
    
    // MARK .- DTDreamDetailsProtocol
    func refreshController() {
        self.dreamList = DTDatabaseService.sharedInstance.retreivedreamList()
        self.tableView.reloadData()
    }
    
}
