//
//  EQMotionDectectorService.swift
//  DreamTracker
//
//  Created by Leopoldo G Vargas on 2015-12-07.
//  Copyright © 2015 Leopoldo G Vargas. All rights reserved.
//

import Foundation
import CoreMotion
import UIKit

class EQMotionDectectorService {
    
    let motionManager: CMMotionManager = CMMotionManager()
    var accelerationX: CGFloat = CGFloat()
    
    init() {
        
    }
    
    ///////////////////////////Accelerometer///////////////////////////////////////
    func startAccelerometer() {
        
        let _:CMAccelerometerData!
        let _:NSError!
        
        if(motionManager.isAccelerometerAvailable){
            motionManager.startAccelerometerUpdates(to: OperationQueue.current!, withHandler: {
                accelerometerData,error in
                
                let acceleration = accelerometerData!.acceleration
                self.accelerationX = CGFloat(acceleration.x)
                
                // Rotate view
                //                let rotation = atan2(accelerometerData!.acceleration.x, accelerometerData!.acceleration.y) - M_PI
                //                self.view!.transform = CGAffineTransformMakeRotation(CGFloat(rotation))
                
                self.refineData((accelerometerData?.description)! as NSString)
            })
        }
    }
    
    func stopAccelerometer() {
        motionManager.stopAccelerometerUpdates()
    }
    
    ///////////////////////////Gryroscope///////////////////////////////////////
    func startGyro() {
        if motionManager.isGyroAvailable {
            motionManager.gyroUpdateInterval = 0.1
            motionManager.startGyroUpdates()
            
            let queue = OperationQueue.main
            motionManager.startGyroUpdates(to: queue) {
                (data, error) in
                
                self.refineData((data?.description)! as NSString)
            }
        }
    }
    
    func stopGyro() {
        motionManager.stopGyroUpdates()
    }

    
    func refineData(_ data: NSString) {
        print(data)
        
        _ = NSMakeRange(0, data.length)
        
        // self.toDoItems.append(data as String)
    }

}
